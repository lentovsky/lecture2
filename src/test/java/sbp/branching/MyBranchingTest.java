package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

public class MyBranchingTest {

    @Test
    public void maxInt_test () {

        final int i1 = 3;
        final int i2 = 5;

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsMock);

        Assertions.assertEquals(i2, myBranching.maxInt(i1, i2));
        Assertions.assertEquals(i2, myBranching.maxInt(i2, i1));
    }

    @Test
    public void ifElseExample () {

        boolean func2 = true;
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(func2);
        MyBranching myBranching = new MyBranching(utilsMock);

        Assertions.assertTrue(myBranching.ifElseExample());

        func2 = false;
        Assertions.assertTrue(myBranching.ifElseExample());

    }
}
